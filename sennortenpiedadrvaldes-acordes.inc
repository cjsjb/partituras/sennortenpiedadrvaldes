\context ChordNames
	\chords {
	\transpose g' g' {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		g2. g2.

		% sennor...
		g2. g2. a2.:m/fis b2.:7
		e2.:m e2.:m/d c2. g2.:/b
		a2.:m a2.:/g d2.:/fis d2.:7

		% cristo...
		g2. g2. a2.:m/fis b2.:7
		e2.:m e2.:m/d c2. g2.:/b
		a2.:m a2.:/g d2.:/fis d2.:7

		% sennor...
		g2. g2. a2.:m/fis b2.:7
		e2.:m e2.:m/d c2. g2.:/b
		a2.:m a2.:/g d2.:/fis d2.:7
		g2. g2.
	}
	}
