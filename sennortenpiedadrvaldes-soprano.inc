\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "treble"
	\transpose g' g' {
		\key g \major

		R2.  |
		r4. r8 r b'  |
		b' 2.  |
		r8 b' b' b' b' b'  |
%% 5
		b' 8 a' 4 ~ a' 4. ~  |
		a' 4. r8 r a'  |
		g' 2.  |
		r8 g' g' g' g' g'  |
		g' 8 e' 4 ~ e' 4. ~  |
%% 10
		e' 4. r8 r e'  |
		e' 2 fis' 8 g'  |
		e' 2 fis' 8 g'  |
		a' 8 a' 4 ~ a' 4. ~  |
		a' 4. r  |
%% 15
		b' 8 b' 4 ~ b' 4.  |
		r8 b' b' b' b' b'  |
		b' 8 a' 4 ~ a' 4. ~  |
		a' 4. r  |
		g' 8 g' 4 ~ g' 4.  |
%% 20
		r8 g' g' g' g' g'  |
		g' 8 e' 4 ~ e' 4. ~  |
		e' 4. r  |
		e' 8 e' 4 r8 fis' g'  |
		e' 4. r8 fis' g'  |
%% 25
		a' 4. ( b' 8 c'' b' )  |
		a' 4. r8 r b'  |
		b' 2.  |
		r8 b' b' b' b' b'  |
		b' 8 a' 4 ~ a' 4. ~  |
%% 30
		a' 4. r8 r a'  |
		g' 2.  |
		r8 g' g' g' g' g'  |
		g' 8 e' 4 ~ e' 4. ~  |
		e' 4. r8 r e'  |
%% 35
		e' 2 fis' 8 g'  |
		e' 2 fis' 8 g'  |
		a' 4. ( b' 8 c'' b' )  |
		a' 4. ( b'  |
		g' 2. )  |
%% 40
		R2.  |
		\bar "|."
	} }

	\new Lyrics \lyricsto "voz-soprano" {
		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros. __

		Cris -- to, __ ten pie -- dad de no -- so -- tros, __
		Cris -- to, __ ten pie -- dad de no -- so -- tros, __
		Cris -- to, ten pie -- dad de no -- so -- tros. __

		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros. __
	}
>>
