\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "treble_8"
	\transpose g' g' {
		\key g \major

		R2.  |
		r4. r8 r b  |
		b 2.  |
		r8 b b b b b  |
%% 5
		b 8 a 4 ~ a 4. ~  |
		a 4. r8 r a  |
		g 2.  |
		r8 g g g g g  |
		g 8 e 4 ~ e 4. ~  |
%% 10
		e 4. r8 r e  |
		e 2 fis 8 g  |
		e 2 fis 8 g  |
		a 8 a 4 ( b 4.  |
		d' 4. ) r  |
%% 15
		r4. d' 8 d' 4  |
		r8 d' d' d' d' d'  |
		d' 8 c' 4 ( ~ c' 4.  |
		b 4. ) r  |
		b 8 b 4 ~ b 4.  |
%% 20
		r8 b b b b b  |
		b 8 g 4 ~ g 4. ~  |
		g 4. r  |
		g 8 g 4 r8 a b  |
		g 4. r8 fis g  |
%% 25
		a 4. ( b 8 c' b )  |
		d' 4. r8 r b  |
		b 4. ( d' )  |
		r8 d' d' d' d' d'  |
		d' 8 c' 4 ( ~ c' 4.  |
%% 30
		b 4. ) r8 r b  |
		b 2.  |
		r8 b b b b b  |
		b 8 g 4 ~ g 4. ~  |
		g 4. r8 r g  |
%% 35
		g 2 a 8 b  |
		g 2 fis 8 g  |
		a 4. ( b 8 c' b )  |
		d' 4. ( c'  |
		b 2. )  |
%% 40
		R2.  |
		\bar "|."
	} }

	\new Lyrics \lyricsto "voz-tenor" {
		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros. __

		Cris -- to, ten pie -- dad de no -- so -- tros, __
		Cris -- to, __ ten pie -- dad de no -- so -- tros, __
		Cris -- to, ten pie -- dad de no -- so -- tros. __

		Se -- ñor, __ ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros, __
		Se -- ñor, ten pie -- dad de no -- so -- tros. __
	}
>>
